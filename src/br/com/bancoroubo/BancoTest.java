package br.com.bancoroubo;

import javax.swing.JOptionPane;

public class BancoTest {

	public static void main(String[] args) {
		Conta minha = new Conta(1, "conta corrente", 2000, new Cliente("Irizinha", "23532", "45643", Sexo.FEMININO));
		// minha.setNumero(1);
		// minha.setTipo("conta corrente");
		// minha.setSaldo(2000);
		Conta.saldoBanco = 100.00;

		minha.saca(300);

		minha.exibeSaldo();
		minha.deposita(90);
		minha.exibeSaldo();

		JOptionPane.showMessageDialog(null, Conta.saldoBanco);
		JOptionPane.showMessageDialog(null, "DADOS DA CONTA");
		JOptionPane.showMessageDialog(null, "N�: " + minha.getNumero());
		JOptionPane.showMessageDialog(null, "Tipo: " + minha.getTipo());
		JOptionPane.showMessageDialog(null, "Saldo: " + minha.getSaldo());
		JOptionPane.showMessageDialog(null, "Titular: " + minha.getCliente().getNome());
		JOptionPane.showMessageDialog(null, "CPF: " + minha.getCliente().getCpf());
		JOptionPane.showMessageDialog(null, "RG: " + minha.getCliente().getRg());
	
	}

}