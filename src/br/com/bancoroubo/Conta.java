package br.com.bancoroubo;

import javax.swing.JOptionPane;

public class Conta {

	public static double saldoBanco;

	// atributos
	private int numero;
	private TipoConta tipoConta;
	private double saldo;
	private Cliente cliente;

	// construtor

	public Conta(int numero, TipoConta tipoConta, double saldo, Cliente cliente) {
		this.numero = numero;
		this.tipoConta = tipoConta;
		this.saldo = saldo;
		this.cliente = cliente;
	}

	// Getters&Setters
	public int getNumero() {
		return numero;
	}

	public static double getSaldoBanco() {
		return saldoBanco;
	}

	public static void setSaldoBanco(double saldoBanco) {
		Conta.saldoBanco = saldoBanco;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	// m�todos
	public void exibeSaldo() {
		JOptionPane.showMessageDialog(null, "Saldo: " + this.saldo);
	}

	public void saca(double valor) {
		saldo -= valor;
		Conta.saldoBanco += valor;
	}

	public void deposita(double valor) {
		saldo += valor;
		Conta.saldoBanco -= valor;
	}

	public void transferePara(Conta destino, double valor) {
		this.saca(valor);
		destino.deposita(valor);
	}

}