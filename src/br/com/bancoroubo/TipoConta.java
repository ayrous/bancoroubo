package br.com.bancoroubo;

public enum TipoConta {

	CORRENTE("Conta Corrente"), SALARIO("Conta Salario"), POUPANCA("Conta Poupan�a");

	public String titulo;

	TipoConta(String titulo) {
		this.titulo = titulo;
	}

}
